package com.itconsortium.cal.banking;

import javax.persistence.Basic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class CalBankBankingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalBankBankingApplication.class, args);
	}
	
	@Bean
	RestTemplate restTemplate(){
		return new RestTemplate();
	}
}
