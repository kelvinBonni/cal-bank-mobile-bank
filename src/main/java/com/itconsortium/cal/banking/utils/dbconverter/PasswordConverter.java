package com.itconsortium.cal.banking.utils.dbconverter;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.tomcat.util.codec.binary.Base64;
@Converter
public class PasswordConverter implements AttributeConverter<String, String> {

	private static final String ALGORITHM = "AES/ECB/PKCS5Padding";
	private static final byte[] KEY = "MySuperSecretKey".getBytes();
	@Override
	public String convertToDatabaseColumn(String password) {
		if(password == null)
			return null;
		// do some encryption
		Key key = new SecretKeySpec(KEY, "AES");
		try {
			Cipher c = Cipher.getInstance(ALGORITHM);
			c.init(Cipher.ENCRYPT_MODE, key);
			//	         return Base64.encodeBytes(c.doFinal(attribute.getBytes()));
			return Base64.encodeBase64String(c.doFinal(password.getBytes()));//Bytes(c.doFinal(attribute.getBytes()));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String convertToEntityAttribute(String dbData) {
		if(dbData == null)
			return null;
		// do some decryption
		Key key = new SecretKeySpec(KEY, "AES"); 
		try {
			Cipher c = Cipher.getInstance(ALGORITHM);
			c.init(Cipher.DECRYPT_MODE, key);
			return new String(c.doFinal(Base64.decodeBase64(dbData)));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
