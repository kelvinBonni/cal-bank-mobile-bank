package com.itconsortium.cal.banking.model;
public enum CustomerStatus {
	ACTIVE,
	INACTIVE
}