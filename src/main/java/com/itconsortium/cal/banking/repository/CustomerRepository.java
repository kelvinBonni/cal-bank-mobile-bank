package com.itconsortium.cal.banking.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itconsortium.cal.banking.model.Customer;
@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {
	public Customer findByMsisdn(String msisdn);
	public Customer findById(Long id);

}
