package com.itconsortium.cal.banking.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itconsortium.cal.banking.model.CustomerAccount;

@Repository
public interface CustomerAccountRepository extends CrudRepository<CustomerAccount, Long>{
	CustomerAccount findByMsisdn(String msisdn);
}
