package com.itconsortium.cal.banking.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.itconsortium.cal.banking.model.Customer;
import com.itconsortium.cal.banking.model.CustomerAccount;
import com.itconsortium.cal.banking.repository.CustomerRepository;
import com.itconsortium.cal.banking.repository.CustomerAccountRepository;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/account")
@Slf4j
public class CustomerAccountController {
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	CustomerAccountRepository customerAccountRepository;
	@Autowired
	RestTemplate restTemplate;

	@PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
	public Customer create(@RequestBody CustomerAccount customerAccount) {
		Customer customer = customerRepository.findByMsisdn(customerAccount.getMsisdn());
		customer.addCustomerAccount(customerAccount);
		Customer result = customerRepository.save(customer);
		return result;

	}

	@GetMapping("/find/{msisdn}")
	public Customer findByMsisdn(@PathVariable String msisdn, HttpServletResponse response) {
		return customerRepository.findByMsisdn(msisdn);
	}

	@DeleteMapping("/delete/{msisdn}")
	public String delete(@PathVariable String msisdn, HttpServletResponse response) {
		Customer customer = customerRepository.findByMsisdn(msisdn);
		customerRepository.delete(customer);
		String deleteMessage = "Customer with id:" + customer.getId() + "has been deleted";
		return deleteMessage;
	}
}
